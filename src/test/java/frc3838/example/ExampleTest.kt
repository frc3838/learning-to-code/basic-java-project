package frc3838.example

import mu.KotlinLogging
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

class ExampleTest
{
    private val logger = KotlinLogging.logger {}
    @Test
    fun simpleTest()
    {
        println("java.version: ${System.getProperty("java.version")}")
        println("java.vendor:  ${System.getProperty("java.vendor")}")
        assertEquals(4, (2 + 2))
    }
}