
This project is set up to use Gradle, a build tool, to manage
its dependencies (i.e. libraries), and the compile classpath.

The project is configured for both Java and Kotlin code.

## To compile/build from the Command Line / Terminal

From the project's root directory, run the command:

    ./gradlew build

## Adding a Library/Dependencies

To add a library or dependency to the project, open the 
`build.gradle.kts` file, scroll down to the `dependencies`
block, and add an `implementation` call. For example:

    implementation("org.slf4j:slf4j-api:2.0.5")

Ideally, dependencies should be added via their
GAV (Group:Artifact:Version) coordinates. You can search 
for them at https://search.maven.org/ then copy and paste 
the "Gradle Kotlin DSL" declaration below If you have trouble 
with that, then put the JAR in the `libs` directory in this 
project's root directory, then add the following, changing the
JAR file name, to the `dependencies` block in the `build.gradle.kts`
file:
    
    implementation(files("libs/some-library.jar"))
