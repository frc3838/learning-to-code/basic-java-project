import org.jetbrains.gradle.ext.*
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import org.jetbrains.kotlin.gradle.plugin.getKotlinPluginVersion

val javaVersion: JavaVersion = JavaVersion.VERSION_17
val kotlinVersion by extra { project.getKotlinPluginVersion() }

plugins {
    java
    kotlin("jvm") version "1.7.21"
    id("org.jetbrains.gradle.plugin.idea-ext") version "1.1.7"
    id("io.spring.dependency-management") version "1.1.0"
}

group = "io.gitlab.frc3838.examples"
version = "0.0.1"


repositories {
    mavenCentral()
}

dependencies {
    
    // ADDING LIBRARIES/DEPENDENCIES
    // Ideally, dependencies should be added via their 
    // GAV (Group, Artifact, Version) coordinates.
    // You can search for them at https://search.maven.org/
    // then copy and paste the "Gradle Kotlin DSL" declaration below
    // If you have trouble with that, then put the JAR in the 'libs'
    // directory in this project's root directory, then add the
    // following below, changing the JAR file name:
    //     implementation(files("libs/some-library.jar"))
    
    implementation(platform("org.jetbrains.kotlin:kotlin-bom:$kotlinVersion"))
    implementation("org.slf4j:slf4j-api:2.0.5")
    implementation("io.github.microutils:kotlin-logging-jvm:3.0.4")
    implementation("ch.qos.logback:logback-classic:1.4.5")
    implementation("org.jetbrains:annotations:23.1.0")

    testImplementation(platform("org.junit:junit-bom:5.9.1"))
    testImplementation("org.junit.jupiter:junit-jupiter-api")
    testImplementation("org.junit.jupiter:junit-jupiter-params")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine")
    testImplementation(kotlin("test-junit5"))
}




java {
    toolchain {
        languageVersion.set(JavaLanguageVersion.of(javaVersion.toString()))
    }
}


tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = javaVersion.toString()
}

tasks.named<Test>("test") {
    useJUnitPlatform()
}
        

idea {
// https://github.com/JetBrains/gradle-idea-ext-plugin/wiki
    module {
        // Download and attach Source & Javadoc JARs for dependencies when available
        isDownloadSources = true
        isDownloadJavadoc = true
    }

    // Kotlin DSL syntax improved greatly in gradle-idea-ext v1.1.3 (Feb 2022). But as of Aug 2, 2022 the Kotlin DSL is still not documented (fully). But as noted in https://github.com/JetBrains/gradle-idea-ext-plugin/issues/44
    // Some examples exist in the unit test code here: https://github.com/JetBrains/gradle-idea-ext-plugin/blob/master/src/test/groovy/TestIdeaExtPluginOnKotlinBuildFile.groovy

    // Be sure to: import org.jetbrains.gradle.ext.*
    project {
        settings {
            encodings {
                encoding = "UTF-8"
                bomPolicy = EncodingConfiguration.BomPolicy.WITH_NO_BOM
                properties {
                    encoding = "UTF-8" // UTF-8 Properties files supported as of Java 5
                    bomPolicy = EncodingConfiguration.BomPolicy.WITH_NO_BOM
                    transparentNativeToAsciiConversion = false
                }
            }
        }
    }
}
